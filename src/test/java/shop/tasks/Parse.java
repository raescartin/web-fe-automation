package shop.tasks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Parse {
	private static final Logger LOGGER = LoggerFactory.getLogger(Parse.class);
	
	public static int receipt(String purchaseReceipt) {
		
		String[] receipt = purchaseReceipt.split("\\r?\\n");
		int purchaseId = Integer.parseInt(receipt[0].replaceAll("[\\D]", ""));
		int purchaseAmount = Integer.parseInt(receipt[1].replaceAll("[\\D]", ""));
		LOGGER.info("Purchase id: {}", purchaseId);
		LOGGER.info("Purchase amount: {}", purchaseAmount);
		
		return purchaseAmount;
	}

}
