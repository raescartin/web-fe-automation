package shop.navigation.pages;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.annotations.DefaultUrl;

@DefaultUrl("https://www.demoblaze.com/index.html")
public class HomePage extends PageObject {
	WebElementFacade productLink;

	public boolean productLinkIsVisible() {
		return productLink.isVisible();
	}
}
