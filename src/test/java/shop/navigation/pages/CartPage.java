package shop.navigation.pages;

import net.serenitybdd.annotations.DefaultUrl;
import org.openqa.selenium.By;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

@DefaultUrl("https://www.demoblaze.com/cart.html")
public class CartPage extends PageObject {
	@FindBy(id = "name")
	WebElementFacade nameField;
	WebElementFacade countryField;
	WebElementFacade cityField;
	@FindBy(id = "card")
	WebElementFacade creditCardField;
	WebElementFacade monthField;
	WebElementFacade yearField;
	WebElementFacade purchasedParagraph;

	public void typeNameField(String name) {
		enter(name).into(nameField);
	}

	public void typeCardField(String cardNumber) {
		enter(cardNumber).into(nameField);
	}

	public WebElementFacade getPurchasedParagraph() {
		return find(By.cssSelector("p.lead.text-muted"));
	}
}
