package shop.navigation.helpers;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

public class Navigate {
	public static Performable to(String searchTerm) {
		return Task.where("{0} navigates to '" + searchTerm + "'", Click.on(By.linkText(searchTerm)));
	}
}
