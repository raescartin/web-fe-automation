package shop.navigation.helpers;

import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import shop.navigation.pages.HomePage;

public class NavigateTo {
	public static Performable theHomePage() {
		return Task.where("{0} opens the home page", Open.browserOn().the(HomePage.class));
	}
}
