package shop.targets;

import org.openqa.selenium.By;

import net.serenitybdd.screenplay.targets.Target;

public class Cart {
	public static By productLocator(String product) {
		return By.xpath("//tr[td[text()='" + product + "']]");
	}
	
	public static Target has(String product) {
		return Target.the("product in cart").located(productLocator(product));
	}
	
	public static Target delete(String product) {
		return Target.the("delete").located(By.xpath("//tr[td[text()='" + product + "']]//a[text()='Delete']"));
	}
}
