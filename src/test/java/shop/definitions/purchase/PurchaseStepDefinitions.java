package shop.definitions.purchase;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.ensure.Ensure;
import net.serenitybdd.screenplay.targets.Target;
import net.serenitybdd.screenplay.waits.WaitUntil;
import shop.navigation.helpers.Navigate;
import shop.navigation.helpers.NavigateTo;
import shop.questions.PurchaseParagraph;
import shop.targets.Cart;
import shop.tasks.Parse;

public class PurchaseStepDefinitions {

	@Before
	public void openTheApplication() {
		OnStage.aNewActor().wasAbleTo(NavigateTo.theHomePage());
	}

	@Given("{actor} navigated to {string}")
	public void navigatedTo(Actor actor, String term) {
		actor.attemptsTo(Navigate.to(term));
	}

	@When("{actor} adds to cart {string}")
	public void addsToCart(Actor actor, String term) {
		actor.attemptsTo(
				Click.on(By.linkText(term)).afterWaitingUntilEnabled(),
				Click.on(By.linkText("Add to cart")).afterWaitingUntilEnabled()
				);
		WaitUntil.the(ExpectedConditions.alertIsPresent()).performAs(actor);
		BrowseTheWeb.as(actor).getAlert().accept();
	}

	@When("{actor} deletes from cart {string}")
	public void deletesFromCart(Actor actor, String product) {
		actor.attemptsTo(Navigate.to("Cart"),
				Click.on(Cart.delete(product))
						.afterWaitingUntilEnabled());
	}

	@When("{actor} places the order")
	public void placesTheOrder(Actor actor) {
		actor.attemptsTo(
				Click.on(By.xpath("//button[text()='Place Order']")).afterWaitingUntilEnabled(),
				Enter.theValue(actor.getName()).into(Target.the("nameField").located(By.id("name"))),
				Enter.theValue("Spain").into(Target.the("countryField").located(By.id("country"))),
				Enter.theValue("Zaragoza").into(Target.the("cityField").located(By.id("city"))),
				Enter.theValue("Mayo").into(Target.the("monthField").located(By.id("month"))),
				Enter.theValue("2022").into(Target.the("yearField").located(By.id("year"))),
				Enter.theValue("no card").into(Target.the("cardField").located(By.id("card"))),
				Click.on(By.xpath("//button[text()='Purchase']")).afterWaitingUntilEnabled()
				);
	}

	@Then("{actor} cart should have {string}")
	public void cartShouldHave(Actor actor, String product) {
		actor.attemptsTo(Navigate.to("Cart"), Ensure
				.that(Cart.has(product)).isEnabled());
	}

	@Then("{actor} cart should not have {string}")
	public void cartShouldNotHave(Actor actor, String product) {
		WaitUntil.the(ExpectedConditions.invisibilityOfElementLocated(Cart.productLocator(product)))
				.performAs(actor);
		actor.attemptsTo(
				Navigate.to("Cart"),
				Ensure.that(Cart.has(product)).isNotDisplayed()
						);
	}

	@Then("{actor} purchase amount equals {int}")
	public void purchaseAmountEquals(Actor actor, int amount) {
		String purchaseReceipt = PurchaseParagraph.value().answeredBy(actor);
		actor.attemptsTo(
				Ensure.that(Parse.receipt(purchaseReceipt)).isEqualTo(amount),
				Click.on(By.xpath("//button[text()='OK']")).afterWaitingUntilEnabled()
				);
	}
}
