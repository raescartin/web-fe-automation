package shop.definitions.navigation;

import org.openqa.selenium.By;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.ensure.Ensure;
import net.serenitybdd.screenplay.targets.Target;
import shop.navigation.helpers.Navigate;
import shop.navigation.helpers.NavigateTo;

public class NavigationStepDefinitions {

	@Given("{actor} navigated to the home page")
	public void shopingThings(Actor actor) {
		actor.wasAbleTo(NavigateTo.theHomePage());
	}

	@When("{actor} navigates to {string}")
	public void navigatesTo(Actor actor, String term) {
		actor.attemptsTo(Navigate.to(term));
	}

	@Then("{actor} should see {string}")
	public void should_see(Actor actor, String term) {
		actor.attemptsTo(Ensure.that(Target.the("product link").located(By.linkText(term))).isDisplayed());
	}

	@Then("{actor} should not see {string}")
	public void should_not_see(Actor actor, String term) {
		actor.attemptsTo(Ensure.that(Target.the("product link").located(By.linkText(term))).isNotDisplayed());
	}
}
