package shop.questions;

import org.openqa.selenium.By;

import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.targets.Target;

public class PurchaseParagraph {
	public static Target USER_NAME = Target.the("User name").located(By.cssSelector("p.lead.text-muted"));
	
    public static Question<String> value() {
        return actor -> USER_NAME.resolveFor(actor).getText();
    }

}
