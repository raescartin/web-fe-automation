Feature: Purchase

  Scenario: Add Sony Laptop to cart
    Given Alice navigated to "Laptops"
    When she adds to cart "Sony vaio i5"
    Then her cart should have "Sony vaio i5"
    
  Scenario: Add Dell Laptop to cart
    Given Alice navigated to "Laptops"
    When she adds to cart "Dell i7 8gb"
    Then her cart should have "Dell i7 8gb"
    And her cart should have "Sony vaio i5"
    
  Scenario: Delete Dell Laptop from cart
    Given Alice navigated to "Cart"
    When she deletes from cart "Dell i7 8gb"
    Then her cart should have "Sony vaio i5"
    And her cart should not have "Dell i7 8gb"
    
  Scenario: Place order
    Given Alice navigated to "Cart"
    When she places the order
    Then her purchase amount equals 790