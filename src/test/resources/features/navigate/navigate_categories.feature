Feature: Navigate categories

  Scenario: Navigate Phones
    Given Alice navigated to the home page
    When she navigates to "Phones"
    Then she should see "Iphone 6 32gb"
    And she should not see "MacBook air"
    And she should not see "Apple monitor 24"
    
  Scenario: Navigate Laptops
    Given Alice navigated to the home page
    When she navigates to "Laptops"
    Then she should see "MacBook air"
    And she should not see "Iphone 6 32gb"
    And she should not see "Apple monitor 24"
    
  Scenario: Navigate Monitors
    Given Alice navigated to the home page
    When she navigates to "Monitors"
    Then she should see "Apple monitor 24"
    And she should not see "Iphone 6 32gb"
    And she should not see "MacBook air"