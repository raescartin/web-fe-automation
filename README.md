# Adidas WEB testing challenge

This is a simple project using Serenity with Cucumber and RestAssured

## Executing the tests
To run the sample project, you can either just run the `CucumberTestSuite` test runner class, or run either `mvn verify` or `gradle test` from the command line.

## Requirements
- Chrome has to be installed and updated

## TODO:
- StaleElementException may happen if the headless mode is disabled, we should implement a click method that captures the exception an retries to click
- More Screenplay pattern can be used